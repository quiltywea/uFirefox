// This file can be used to configure global preferences for Firefox
// Example: Homepage
pref("browser.urlbar.placeholderName", "DuckDuckGo");
pref("browser.urlbar.placeholderName.private", "DuckDuckGo");
pref("browser.startup.homepage", "about:config");
pref("browser.shell.checkDefaultBrowser", false);
pref("dom.w3c_touch_events.enabled", 1);
pref("apz.allow_zooming", true);
pref("apz.allow_double_tap_zooming", true);

