#package specific information for the desktop file
sed -i "s/@name@/Firefox Web Browser/" ${INSTALL_DIR}/*.desktop
sed -i "s/@comment@/Browse the World Wide Web/" ${INSTALL_DIR}/*.desktop
sed -i "s/@keywords@/Internet;WWW;Browser;Web;Explorer/" ${INSTALL_DIR}/*.desktop
sed -i "s|@iconpath@|share/icons/hicolor/symbolic/apps/firefox-symbolic.svg|" ${INSTALL_DIR}/*.desktop
sed -i "s/@categories@/GNOME;GTK;Network;WebBrowser;/" ${INSTALL_DIR}/*.desktop
#a short description for the package
sed -i "s/@description@/firefox is a webbrowser/" ${INSTALL_DIR}/manifest.

#debug: uncomment below lines to output information to the terminal
#echo "click path: ${CLICK_PATH}"
#echo "build dir: ${BUILD_DIR}"
#echo "click ld library path: ${CLICK_LD_LIBRARY_PATH}"
#echo "install dir: ${INSTALL_DIR}"
#echo
#echo "waiting: press any key to continue...."
#read -p "waiting...."

#adapt to fit the needs of the package
rm ${CLICK_PATH}/firefox
cd ${CLICK_PATH}
ln -s ../firefox/firefox .
rm ${CLICK_LD_LIBRARY_PATH}/firefox/browser/defaults/preferences/syspref.js
cp ${BUILD_DIR}/syspref.js ${CLICK_LD_LIBRARY_PATH}/firefox/browser/defaults/preferences/syspref.js
